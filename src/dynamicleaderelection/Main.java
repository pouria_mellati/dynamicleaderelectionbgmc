package dynamicleaderelection;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.Random;

import bgmc.BaState;
import bgmc.ModelChecking;
import bgmc.ModelCheckingResult;
import bgmc.actormodelhelpers.ActorModel;
import bgmc.actormodelhelpers.ActorModelState;
import bgmc.actormodelhelpers.ActorState;
import bgmc.actormodelhelpers.Message;
import bgmc.property.Formula;
import bgmc.property.LtlQAwareBaState;
import bgmc.property.parser.PropertyParser;

public class Main {
	// The first arg to the app is the path to the property file.
	public static void main(String[] args) throws FileNotFoundException {
		if(args.length < 1)
			throw new RuntimeException("The only arg to the app (the path to the property file) was not specified.");
		
		Map<String, ActorState> actors = new HashMap<>(2);
		
		Broadcaster broadcaster = new Broadcaster(10);
		String broadcasterId = "BroadcasterInstance";
		actors.put(broadcasterId, broadcaster);
		
		List<Integer> netIdsSeq = createNetIdsListOfSize(4);
		System.out.println("The sequence of network ids in this model checking execution is:\n" + netIdsSeq.toString());
		NodeSpawner spawner = new NodeSpawner(10, netIdsSeq, broadcasterId);
		spawner.addMessage(new Message("Create"));
		String spawnerId = "SpawnerInstance";
		actors.put(spawnerId, spawner);
		
		List<ActorModelState> initialStates = new LinkedList<>();
		initialStates.add(new ActorModelState(actors, null));
		ActorModel model = new ActorModel(initialStates);
		
		FileReader propertyFile = new FileReader(args[0]);
		Formula propertyFormula = new PropertyParser().parsePropertyAndNegate(propertyFile);
		System.out.println("The negation of the property is:\n" + propertyFormula);
		BaState initPropertyState = LtlQAwareBaState.fromFormula(propertyFormula);
		
		System.out.println("Running the model checker ...");
		long milisBeforeRun = System.currentTimeMillis();
		ModelCheckingResult result = new ModelChecking(model, initPropertyState).run();
		System.out.println("Took " + (System.currentTimeMillis() - milisBeforeRun) / 1000.0 + " secs.");
		if(result.wasModelValid())
			System.out.println("The model is valid!");
		else {
			System.out.println("The model was invalid!");
		}
	}
	
	private static List<Integer> createNetIdsListOfSize(int numIdsToCreate) {
		Set<Integer> createdIds = new HashSet<>(numIdsToCreate);
		
		Random r = new Random();
		while(createdIds.size() < numIdsToCreate)
			createdIds.add(r.nextInt());
		
		return new LinkedList<Integer>(createdIds);
	}
}

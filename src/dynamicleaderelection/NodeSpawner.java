package dynamicleaderelection;

import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import bgmc.ObjectVal;
import bgmc.actormodelhelpers.ActorState;
import bgmc.actormodelhelpers.ActorStateSuccessorDelta;
import bgmc.actormodelhelpers.Message;
import bgmc.actormodelhelpers.actordefinition.AbstractActorState;
import bgmc.actormodelhelpers.actordefinition.MessageAggregator;

public class NodeSpawner extends AbstractActorState {
	List<Integer> netIds;	// This is immutable and shared amongst the various states of this actor.
	String broadcaster;	  // This is immutable and shared amongst the various states of this actor.
	int numNodesToCreate;	// This is immutable and shared amongst the various states of this actor.
	int numNodesCreated = 0;
	
	public NodeSpawner(int maxQLength, List<Integer> netIds, String broadcaster) {
		super(maxQLength, "NodeSpawner");
		this.netIds = netIds;
		this.numNodesToCreate = this.netIds.size();
		this.broadcaster = broadcaster;
	}
	
	@Override
	public List<ActorStateSuccessorDelta> getSuccessorDeltas(String self) {
		List<ActorStateSuccessorDelta> deltas = new LinkedList<>();
		Message msg = messageQ.peek();
		
		if(msg.name.equals("Create")) {
			NodeSpawner my = (NodeSpawner)this.clone();
			my.messageQ.remove();
			
			MessageAggregator messenger = new MessageAggregator();
			Map<String, ActorState> newlyInstantiatedActors = new HashMap<>(0);
			
			if(my.numNodesCreated < my.numNodesToCreate) {
				boolean newNodeIsLeader = my.numNodesCreated == 0;	// The first node must be a leader, as we need a leader at start-up.
				Node newNode = new Node(10, my.netIds.get(my.numNodesCreated), my.broadcaster, newNodeIsLeader);
				String newNodeId = "NodeInstance" + String.valueOf(my.numNodesCreated);
				
				newlyInstantiatedActors.put(newNodeId, newNode);
				
				List<Object> registerMsgPayload = new LinkedList<>();
				registerMsgPayload.add(newNodeId);
				messenger.send(my.broadcaster, new Message("Register", registerMsgPayload));
				
				my.numNodesCreated += 1;
			}
			
			messenger.send(self, new Message("Create"));
			
			deltas.add(new ActorStateSuccessorDelta(my, newlyInstantiatedActors, messenger.sentMessages));
		} else
			throw new RuntimeException("Actor of class " + getClassId() + " received an unsupported message: " + msg.name);
		
		return deltas;
	}

	@Override
	public ObjectVal getNonActorField(String fieldName) {
		throw new UnsupportedOperationException("Accessing the non-actor fields of a NodeSpawner is not supported.");
	}

	@Override
	public String tryGetActorFieldId(String fieldName) {
		throw new UnsupportedOperationException("Accessing the actor fields of a NodeSpawner is not supported.");
	}

	@Override
	protected boolean actorEquals(Object thatObj) {
		return numNodesCreated == ((NodeSpawner)thatObj).numNodesCreated;
	}

	@Override
	protected int actorHashCode() {
		return 31 + numNodesCreated;
	}
}
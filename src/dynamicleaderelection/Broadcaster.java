package dynamicleaderelection;

import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;

import bgmc.ObjectVal;
import bgmc.actormodelhelpers.ActorState;
import bgmc.actormodelhelpers.ActorStateSuccessorDelta;
import bgmc.actormodelhelpers.Message;
import bgmc.actormodelhelpers.actordefinition.AbstractActorState;
import bgmc.actormodelhelpers.actordefinition.MessageAggregator;

public class Broadcaster extends AbstractActorState {
	List<String> nodes = new LinkedList<>();
	
	public Broadcaster(int qLength) {
		super(qLength, "Broadcaster");
	}

	@Override
	public List<ActorStateSuccessorDelta> getSuccessorDeltas(String actorId) {
		List<ActorStateSuccessorDelta> deltas = new LinkedList<>();
		Message msg = messageQ.peek();
		
		Broadcaster my = this.clone();
		my.messageQ.remove();
		
		if(msg.name.equals("Register")) {
			String sender = (String)msg.data.get(0);
			my.nodes.add(sender);
			
			MessageAggregator messenger = new MessageAggregator();
			messenger.send(sender, new Message("Registered"));
			
			deltas.add(new ActorStateSuccessorDelta(my, new HashMap<String, ActorState>(0), messenger.sentMessages));
		} else if(msg.name.equals("I") || msg.name.equals("R")) {
			String sender = (String)msg.data.get(0);
			int senderNetId = (int)msg.data.get(1);
			
			List<Object> broadcastedMsgPayload = new LinkedList<Object>();
			broadcastedMsgPayload.add(senderNetId);
			Message broadcastedMsg = new Message(msg.name, broadcastedMsgPayload);	// Since Messages are not supposed to be mutated, we send the same instance to everyone.
			
			MessageAggregator messenger = new MessageAggregator();
			for(String recipient: my.nodes)
				if(! recipient.equals(sender))
					messenger.send(recipient, broadcastedMsg);
			
			deltas.add(new ActorStateSuccessorDelta(my, new HashMap<String, ActorState>(0), messenger.sentMessages));
		} else
			throw new RuntimeException("Actor of class " + getClassId() + " received an unsupported message: " + msg.name);
		
		return deltas;
	}

	@Override
	public ObjectVal getNonActorField(String fieldName) {
		throw new UnsupportedOperationException("Accessing the non-actor fields of Broadcaster is not supported.");
	}

	@Override
	public String tryGetActorFieldId(String fieldName) {
		return null;
	}

	@Override
	protected boolean actorEquals(Object thatObj) {
		return nodes.equals(((Broadcaster)thatObj).nodes);
	}

	@Override
	protected int actorHashCode() {
		return nodes.hashCode();
	}
	
	@Override
	public Broadcaster clone() {
		Broadcaster c = (Broadcaster)super.clone();
		c.nodes = new LinkedList<>(nodes);
		return c;
	}
}

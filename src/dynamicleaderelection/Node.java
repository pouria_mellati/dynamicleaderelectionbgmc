package dynamicleaderelection;

import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;

import bgmc.ObjectVal;
import bgmc.actormodelhelpers.ActorStateSuccessorDelta;
import bgmc.actormodelhelpers.Message;
import bgmc.actormodelhelpers.ActorState;
import bgmc.actormodelhelpers.actordefinition.AbstractActorState;
import bgmc.actormodelhelpers.actordefinition.MessageAggregator;
import bgmc.actormodelhelpers.actordefinition.SimpleObjectVal;

public class Node extends AbstractActorState{
	int netId;
	String broadcaster;
	int state;
	boolean isLeader;
	
	private static final int UNREGISTERED = 0;
	private static final int CANDIDATE = 1;
	private static final int LEADER = 2;
	private static final int DEFEATED = 3;
	
	public Node(int maxQLength, int netId, String broadcaster, boolean startAsLeader) {
		super(maxQLength, "Node");
		this.netId = netId;
		this.broadcaster = broadcaster;
		if(startAsLeader) {
			this.state = LEADER;
			this.isLeader = true;
		} else {
			this.state = UNREGISTERED;
			this.isLeader = false;
		}
	}
	
	private ActorStateSuccessorDelta nopDelta(Node me) {
		return new ActorStateSuccessorDelta(me, new HashMap<String, ActorState>(0), new HashMap<String, List<Message>>(0));
	}
	
	private Message createIOrRMsg(String IorR, String self, int netId) {
		List<Object> msgPayload = new LinkedList<>();
		msgPayload.add(self);
		msgPayload.add(netId);
		return new Message(IorR, msgPayload);
	}
	
	@Override
	public List<ActorStateSuccessorDelta> getSuccessorDeltas(String self) {
		List<ActorStateSuccessorDelta> deltas = new LinkedList<>();
		Message msg = messageQ.peek();
		
		Node my = (Node)this.clone();
		my.messageQ.remove();
		
		if(my.state == UNREGISTERED) {
			if("Registered".equals(msg.name)) {
				my.state = CANDIDATE;
				
				MessageAggregator messenger = new MessageAggregator();
				messenger.send(my.broadcaster, createIOrRMsg("I", self, my.netId));
				
				deltas.add(new ActorStateSuccessorDelta(my, new HashMap<String, ActorState>(0), messenger.sentMessages));
			} else
				deltas.add(nopDelta(my));
		} else if(my.state == CANDIDATE) {
			if(msg.name.equals("I"))
				deltas.add(nopDelta(my));
			else if(msg.name.equals("R")) {
				int receivedNetId = (int)msg.data.get(0);
				if(receivedNetId < my.netId) {
					MessageAggregator messenger = new MessageAggregator();
					messenger.send(my.broadcaster, createIOrRMsg("I", self, my.netId));
					deltas.add(new ActorStateSuccessorDelta(my, new HashMap<String, ActorState>(0), messenger.sentMessages));
				} else if(receivedNetId == my.netId) {
					my.state = LEADER;
					my.isLeader = true;
					deltas.add(new ActorStateSuccessorDelta(my, new HashMap<String, ActorState>(0), new HashMap<String, List<Message>>(0)));
				} else if(receivedNetId > my.netId) {
					my.state = DEFEATED;
					deltas.add(new ActorStateSuccessorDelta(my, new HashMap<String, ActorState>(0), new HashMap<String, List<Message>>(0)));
				}	
			} else
				throw new RuntimeException("Actor of class " + getClassId() + " received an unsupported message: " + msg.name);
			
		} else if(my.state == LEADER) {
			if(msg.name.equals("I")) {
				int receivedNetId = (int)msg.data.get(0);
				if(receivedNetId < my.netId) {
					MessageAggregator messenger = new MessageAggregator();
					messenger.send(my.broadcaster, createIOrRMsg("R", self, my.netId));
					deltas.add(new ActorStateSuccessorDelta(my, new HashMap<String, ActorState>(0), messenger.sentMessages));
				} else if(receivedNetId > my.netId) {
					my.state = DEFEATED;
					my.isLeader = false;
					
					MessageAggregator messenger = new MessageAggregator();
					messenger.send(my.broadcaster, createIOrRMsg("R", self, receivedNetId));
					deltas.add(new ActorStateSuccessorDelta(my, new HashMap<String, ActorState>(0), messenger.sentMessages));
				}
			} else if(msg.name.equals("Registered")) {
				// The first node created by the system will be a leader, since the model is designed to work with a pre-existing
				// leader. This Node also needs to be registered with the broadcaster. However, on successful registration, nothing
				// else needs to be done.
				deltas.add(nopDelta(my));
			} else
				throw new RuntimeException("Actor of class " + getClassId() + " received an unsupported message: " + msg.name);
		} else if(my.state == DEFEATED) {
			deltas.add(nopDelta(my));
		}
		
		return deltas;
	}

	@Override
	public ObjectVal getNonActorField(String fieldName) {
		if(fieldName.equals("id"))
			return new SimpleObjectVal("Integer", netId);
		else if(fieldName.equals("isLeader"))
			return new SimpleObjectVal("Boolean", isLeader);
		else
			throw new IllegalArgumentException("Actor " + getClassId() + " does not support field: " + fieldName);
	}

	@Override
	public String tryGetActorFieldId(String fieldName) {
		return null;
	}

	@Override
	protected boolean actorEquals(Object thatObj) {
		Node that = (Node)thatObj;
		return netId == that.netId && broadcaster.equals(that.broadcaster) && state == that.state && isLeader == that.isLeader;
	}

	@Override
	protected int actorHashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result
				+ ((broadcaster == null) ? 0 : broadcaster.hashCode());
		result = prime * result + (isLeader ? 1231 : 1237);
		result = prime * result + netId;
		result = prime * result + state;
		return result;
	}
	
	@Override
	public String toString() {
		return "Node(" + netId + ", " + state + ", " + isLeader + ")" + ", Q: " + messageQ.toString();
	}
}
